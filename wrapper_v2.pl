#!/usr/bin/perl


#####################################
## Written by: 					   ##
## Lars Ronn Olsen				   ##	
## Technical University of Denmark ##
## lro@cbs.dtu.dk				   ##
#####################################

## Call modules
use strict;
use Bio::DB::EUtilities;
use Getopt::Long;
use Cwd;


## Call global variables
my $wd = getcwd;
my $job_dir;
my $email = "";
my $term = "";
my $verbose = "f";


## Get commandline options
GetOptions("d=s"=>\$job_dir, "e=s"=>\$email, "t=s"=>\$term, "v=s"=>\$verbose);
$wd .= "/$job_dir";


## Print input parameters if verbose
if ($verbose eq "t") {
	print "runnning with options\njob dir: $job_dir\nemail: $email\nsearch term: $term\n";
}


## Error message for lacking email
if ($email eq "") {
	die "you must enter email address\n";
}


## Make corpora directories
mkdir("$job_dir/corpus");


## Debug comment
if ($verbose eq "t" and $term ne "") {print "fetching query\n"};


## If search term entered, ping NCBI, search for articles matching term
my @pmids;
my @queryids;
my %index;
my $max = 1000 - scalar @pmids;
if ($term ne "") {
	my $factory = Bio::DB::EUtilities->new(-eutil => 'esearch', -db => 'pubmed', -term => $term, -email => $email, -retmax => 1000);
	@queryids = $factory->get_ids;
	if ($verbose eq "t" and $term ne "") {print scalar @queryids, " abstracts were found for search term ", $factory->get_query_translation, "\n"};
	foreach my $id (@queryids) {
		$index{$id} = 3;
	}
}


## Read index file
open IN, "<", "./$job_dir/index.txt" or die $!;
my $line = <IN>;
while (defined $line) {
	chomp $line;
	$line =~ m/(\d*)\t(\d)/;
	$index{$1} = $2;
	push @pmids, $1;
	# print "$index{$1}\n";
	$line = <IN>;
}
close IN;
push(@pmids, @queryids);


## Debug comment
if ($verbose eq "t") {print "fetching pubmed abstracts\n"};


## Split up pmid array in an array of arrays of max 199 abstracts in each
my @temp_pmids = @pmids;
my @pmids_aoa;
push @pmids_aoa, [ splice @temp_pmids, 0, 199 ] while @temp_pmids;


## Fetch abstracts in chunks
my $xml_list = "";
for (my $i=0; $i<scalar @pmids_aoa; $i++) {
	if ($verbose eq "t") {print "\tchunk ", $i+1, " of ", scalar @pmids_aoa, "\n"};

	## Search Pubmed for pmids in index file
	my $factory = Bio::DB::EUtilities->new(-eutil => 'efetch', -db => 'pubmed', -rettype => 'gb', -email => $email, -id => \@{$pmids_aoa[$i]});

	## Save results xml file
	my $file = "./$job_dir/abstracts_$i.xml";
	$factory->get_Response(-file => $file);

	## Wait for xml file to be downloaded
	sleep 1 while ( !(-e "./$job_dir/abstracts_$i.xml") );

	$xml_list .= " $wd/abstracts_$i.xml";
}

## Merge xmls
my $cmd = "cat $xml_list > $wd/abstracts.xml";
system($cmd);
sleep 1 while ( !(-e "./$job_dir/abstracts.xml") );
$cmd = "rm $xml_list";
system($cmd);


## Debug comment
if ($verbose eq "t") {print "extracting texts from xml\n"};


## Define global variables
my %pmids_extracted;
my %titles;
my %pubyear;


## Extract information from abstract xml files
open IN, "<", "./$job_dir/abstracts.xml" or die $!;
$line = <IN>;
while (defined $line) {
	chomp $line;
	my $title;
	my $abstract;
	my $PMID;
	my $language;
	my %mesh;
	if ($line eq "<PubmedArticle>") {
		while (defined $line and $line ne "</PubmedArticle>") {
			if ($line =~ m/<ArticleTitle>(.+)<\/ArticleTitle>/) {
				$title = $1;
				$title =~ s/[^a-zA-Z0-9 ]/ /g
			}
			if ($line =~ m/^\s\s\s\s\s\s\s\s<PMID Version=".+">(.+)<\/PMID>/) {
				$PMID = $1;
			}
			if ($line =~ m/<AbstractText.*>(.+)<\/AbstractText.*>/) {
				$abstract .= " $1";
				$abstract =~ s/-/ /;
				$abstract =~ s/:/ /;
				$abstract =~ s/\*/ /;
			}
			if ($line =~ m/<Language>(.*)<\/Language>/) {
				$language .= lc "$1"
			}
			if ($line =~ m/<DescriptorName UI=".*" MajorTopicYN="\w">(.*)<\/DescriptorName>/) {
				$mesh{$1} = 1;
			}
			if ($line =~ m/<QualifierName UI=".*" MajorTopicYN="\w">(.*)<\/QualifierName>/) {
				$mesh{$1} = 1;
			}	
			if ($line =~ m/<Journal>/) {
				while (defined $line) {
					if ($line =~ m/<\/Journal>/) {
						last;
					}
					if ($line =~ m/<Year>(.*)<\/Year>/) {
						$pubyear{$PMID} = $1;
					}
					$line = <IN>;
					chomp $line;
				}
			}	
			$line = <IN>;
			chomp $line;
		}
		if (defined $abstract and $PMID ne "" and $abstract ne "" and $language eq "eng" and exists $index{$PMID}) {
			open OUT, ">", "./$job_dir/corpus/$PMID.txt" or die $!;
			print OUT lc "$title\n$abstract\n";
			foreach my $key (keys %mesh) {
				print OUT lc "$key ";
			}
			close OUT;
			$pmids_extracted{$PMID} = 1;
			$titles{$PMID} = $title;
		}
	}
	$line = <IN>;
}
close IN;

my %pmid_warning;

if ($verbose eq "t") {print "printing files\n"};

open OUT, ">", "./$job_dir/index_new.txt" or die $!;
open OUTA, ">", "./$job_dir/article_info.txt" or die $!;
foreach my $key (keys %index) {
	if (exists $pmids_extracted{$key}) {
		print OUT "$key\t$index{$key}\n";
		if ($index{$key} == 3) {
			if (exists $pubyear{$key}) {
				print OUTA "$key\t$pubyear{$key}\t$titles{$key}\n";
			}
			else {
				print OUTA "$key\tN/A\t$titles{$key}\n";
			}
		}
	}
	else {
		$pmid_warning{$key} = 1;
	}
}
close OUT;
close OUTA;

my $size_warning = keys %pmid_warning;


## Debug comment
if ($verbose eq "t" and $size_warning > 0) {
	print "Warning: $size_warning out of ", scalar @pmids, " submitted PubMed ID(s) were not found or the abstract was unavailable or not in English.\nThe following PubMed ID(s) were not processed:";
	foreach my $key (sort keys %pmid_warning) {
		print " $key ";
	}
	print "\n";
}


## Call R script
$cmd = "Rscript classify_articles_v3.R -i $wd";


## Debug comment
if ($verbose eq "t") {print "calling \"$cmd\"\n"};
system($cmd)