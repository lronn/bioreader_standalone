# BIOREADER README #

This README describes the use of BioReader as a standalone tool

### SCRIPTS ###

wrapper.pl: imports abstracts by reading the tab separated file index.txt, which contains PubMed IDs and the class of the articles (1=positive, 2=negative, 3=to-be-classified calls NCBI using EUtils, and downloads the abstracts creates folder "corpus" containing the abstracts as txt files, and a number of parameter files for the R script calls the R script classify_articles.R

classify_articles.R trains machine learning algorithms to differentiate between the positive and negative articles class applies best performing algorithm to the abstracts to be classified.

### PACKAGES ###

The perl script uses the following packages (obtainable via CPAN): Bio::DB::EUtilities, Getopt::Long, Cwd

The R script uses the following packages (obtainable via bioconductor and CRAN): optparse, tm, SnowballC, RTextTools, class, e1071, miscTools, wordcloud, scales, rmarkdown

### USAGE ###

Place both scripts in the same directory. Create job directory with name of choice. Create a text file "index.txt" with two columns (tab separated). Column 1: a list of PubMed IDs, column 2: class identifier (1 = class I, 2 = class II, 3 = article to be classified)

Run wrapper.pl with the following options
-d [job directory name]
-e [email address for querying NCBI]
-s [optional search term]
-v [verbose mode for debugging, set to "t"]

### CONTACT ###

Lars R�nn Olsen
Technical University of Denmark
lro@cbs.dtu.dk